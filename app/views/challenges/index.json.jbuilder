json.array!(@challenges) do |challenge|
  json.extract! challenge, :id, :name, :description, :moderation_flag, :status, :terms, :category
  json.url challenge_url(challenge, format: :json)
end
