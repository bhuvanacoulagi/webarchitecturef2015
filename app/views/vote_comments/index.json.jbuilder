json.array!(@vote_comments) do |vote_comment|
  json.extract! vote_comment, :id, :user_id, :comment_id
  json.url vote_comment_url(vote_comment, format: :json)
end
